import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { AppComponent } from './app.component';
import { GpxMapComponent } from './gpx-map/gpx-map.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GpxFormComponent } from './gpx-form/gpx-form.component';
import { HttpClientModule } from '@angular/common/http';
import { MaterialIconsModule } from './material-icons.module';
import { MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatCardModule,
  MatTooltipModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatIconModule
} from '@angular/material';

import 'leaflet-filelayer';
import 'leaflet-editable';
import 'leaflet-geometryutil';
import 'hammerjs';
import { GpxTableComponent } from './gpx-table/gpx-table.component';
import { GpxNavComponent } from './gpx-nav/gpx-nav.component';

@NgModule({
  declarations: [
    AppComponent,
    GpxMapComponent,
    GpxFormComponent,
    GpxTableComponent,
    GpxNavComponent
  ],
  imports: [
    BrowserModule,
    MaterialIconsModule,
    LeafletModule,
    MatIconModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatSortModule,
    MatTableModule,
    MatPaginatorModule,
    MatTooltipModule,
  ],
  providers: [
    { provide: 'DomMapInstance', useValue: window['L'] },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
