import { Component, OnInit, ViewChild, NgZone, OnDestroy } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { GpxServiceService } from '../gpx-service.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-gpx-table',
  templateUrl: './gpx-table.component.html',
  styleUrls: ['./gpx-table.component.scss']
})
export class GpxTableComponent implements OnInit, OnDestroy {

  displayedColumns: string[] = [
    '#',
    'road',
    'distance',
    'startRaceMin',
    'startRaceMax',
    'startTimeMin',
    'startTimeMax',
    'lat',
    'lng',
    'action'
  ];
  columnsToToggle: string[] = [
    'town',
    'state',
    'country',
    'postcode',
  ];
  dataSource: MatTableDataSource<any>;
  tableData: Array<any>[] = [];
  private addressSubN: Subscription;
  private addressRecalcSubN: Subscription;
  private actionSubN: Subscription;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private _gs: GpxServiceService,
    private _z: NgZone) {
    this.dataSource = new MatTableDataSource([]);
  }

  ngOnInit() {
    this.addressSubN = this._gs
      .getAddressPoint()
      .subscribe(this.handleAddressPointAdding.bind(this));
    this.addressRecalcSubN = this._gs
      .getAddressRecalculationData()
      .subscribe(this.handleDataRecalculation.bind(this));
    this.actionSubN = this._gs
      .getAction().subscribe(this.handleAction.bind(this));
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.addressSubN.unsubscribe();
    this.addressRecalcSubN.unsubscribe();
    this.actionSubN.unsubscribe();
  }

  handleAction(action) {
    if (action !== 'tablerows') {
      return;
    }
    if (this.displayedColumns['2'] !== 'town') {
      this.displayedColumns.splice(2, 0, ...this.columnsToToggle);
    } else {
      this.displayedColumns.splice(2, 4);
    }
  }

  handleAddressPointAdding(data) {
    this.tableData.push(data);
    this._z.run(() => {
      this.dataSource = new MatTableDataSource(this.tableData);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  handleDataRecalculation(data) {
    this.tableData = data;
    this._z.run(() => {
      this.dataSource = new MatTableDataSource(this.tableData);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  removePoint(point) {
    this.tableData.splice(this.tableData.indexOf(point), 1);
    this._gs.setAddressPointToRemove(point);
    this._z.run(() => {
      this.dataSource = new MatTableDataSource(this.tableData);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
}
