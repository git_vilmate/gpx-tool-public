import { Component, OnInit, ElementRef, NgZone, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { GpxServiceService } from '../gpx-service.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-gpx-nav',
  templateUrl: './gpx-nav.component.html',
  styleUrls: ['./gpx-nav.component.scss']
})
export class GpxNavComponent implements OnInit, OnDestroy {

  actions: Array<string>[] = [];
  recalculationState = true;
  private actionSubN: Subscription;
  private recalculationSubN: Subscription;
  constructor(
    private _gs: GpxServiceService
  ) { }

  ngOnInit() {
    this.actionSubN = this._gs
      .getAction()
      .subscribe(this.handleActions.bind(this));
    this.recalculationSubN = this._gs
      .getrecalculationState()
      .subscribe(state => {
        this.recalculationState = state;
      });
  }

  ngOnDestroy() {
    this.recalculationSubN.unsubscribe();
    this.actionSubN.unsubscribe();
  }

  buttonClick(type) {
    if (this.actions.indexOf(type) === -1) {
      this.actions.splice(this.actions.length, 0, type);
    } else {
      this.actions.splice(this.actions.indexOf(type), 1);
    }
    this._gs.setAction(type);
  }

  handleActions(actions) {
    for (let i = 0; i < actions.length; i += 1) {
      if (this.actions.indexOf(actions[i]) === -1) {
        this.actions.splice(this.actions.length, 0, actions[i]);
      } else {
        this.actions.splice(this.actions.indexOf(actions[i]), 1);
      }
    }
  }

  uploadFile(e) {
    e.preventDefault();
    // Need to trigger hidden input element from 3d party library
    const elems = document.getElementsByClassName('hidden');
    const evt = new MouseEvent('click', { bubbles: true, cancelable: true, view: window });
    for (let i = 0; i < elems.length; i += 1) {
      // There is only one element with such class name
      elems[i].dispatchEvent(evt);
    }
  }
}
