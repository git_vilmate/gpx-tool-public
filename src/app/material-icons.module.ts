import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { environment } from '../environments/environment';
@NgModule({
  imports: [CommonModule],
  exports: [],
  declarations: [],
  providers: []
})
export class MaterialIconsModule {
  constructor(
    iconRegistry: MatIconRegistry,
     sanitizer: DomSanitizer
    ) {
    iconRegistry.addSvgIcon(
      'upload',
      sanitizer.bypassSecurityTrustResourceUrl(`${environment.iconPath}upload.svg`)
    );
    iconRegistry.addSvgIcon(
      'calc',
      sanitizer.bypassSecurityTrustResourceUrl(`${environment.iconPath}calculator.svg`)
    );
    iconRegistry.addSvgIcon(
      'near',
      sanitizer.bypassSecurityTrustResourceUrl(`${environment.iconPath}near-me.svg`)
    );
    iconRegistry.addSvgIcon(
      'zoomin',
      sanitizer.bypassSecurityTrustResourceUrl(`${environment.iconPath}zoom-in.svg`)
    );
    iconRegistry.addSvgIcon(
      'zoomout',
      sanitizer.bypassSecurityTrustResourceUrl(`${environment.iconPath}zoom-out.svg`)
    );
    iconRegistry.addSvgIcon(
      'polymarkers',
      sanitizer.bypassSecurityTrustResourceUrl(`${environment.iconPath}polymarkers.svg`)
    );
    iconRegistry.addSvgIcon(
      'kmarkers',
      sanitizer.bypassSecurityTrustResourceUrl(`${environment.iconPath}kmmarkers.svg`)
    );
    iconRegistry.addSvgIcon(
      'edit',
      sanitizer.bypassSecurityTrustResourceUrl(`${environment.iconPath}edit.svg`)
    );
    iconRegistry.addSvgIcon(
      'table',
      sanitizer.bypassSecurityTrustResourceUrl(`${environment.iconPath}table.svg`)
    );
    iconRegistry.addSvgIcon(
      'remove',
      sanitizer.bypassSecurityTrustResourceUrl(`${environment.iconPath}remove.svg`)
    );
  }
}
