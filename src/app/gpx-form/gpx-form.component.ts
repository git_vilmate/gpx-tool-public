import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GpxServiceService } from '../gpx-service.service';
import { debounceTime } from 'rxjs/operators';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-gpx-form',
  templateUrl: './gpx-form.component.html',
  styleUrls: ['./gpx-form.component.scss']
})
export class GpxFormComponent implements OnInit, OnDestroy {
  gpxForm: FormGroup;
  formPrevState;
  private formSubN: Subscription;
  constructor(
    private _fb: FormBuilder,
    private _gs: GpxServiceService
  ) { }

  ngOnInit() {
    this.gpxForm = this._fb.group({
      time: new FormControl('09:30', [Validators.required]),
      maxSpeed: new FormControl('40', [Validators.required]),
      minSpeed: new FormControl('45', [Validators.required]),
    });
    this.formPrevState = this.gpxForm.value;
    this._gs.setFormData({ ...this.gpxForm.value, valid: true });
    this.formSubN = this.gpxForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe(data => {
        if (this.gpxForm.valid) {
          if (this.checkFormData(data)) {
            this._gs.setRecalculationState(false);
          }
          this.formPrevState = data;
          this._gs.setFormData({ ...data, valid: true });
        }
      });
  }

  checkFormData(data) {
    let flag = true;
    for (const title in data) {
      if (data.hasOwnProperty(title)) {
        if (data[title] !== this.formPrevState[title]) {
           flag = false;
        }
      }
    }
    return flag;
  }
  ngOnDestroy() {
    this.formSubN.unsubscribe();
  }
}
