import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import * as L from 'leaflet';
import { Subscription } from 'rxjs';

import {GpxServiceService} from '../gpx-service.service';
@Component({
  selector: 'app-gpx-map',
  templateUrl: './gpx-map.component.html',
  styleUrls: ['./gpx-map.component.scss']
})
export class GpxMapComponent implements OnInit, OnDestroy {
  mapInstance;
  mapOptions;
  fileLoader;
  mainRoute;
  pureRoute;
  distanceMarker;
  clickMarker;
  formData;
  markersCounter = 0;
  streetData = [];
  closestMarkers = [];
  nearestModeState = false;
  kmMarkersState = false;
  polyMarkersState = false;
  editPolylineState = false;
  private formDataSubN: Subscription;
  private actionSubN: Subscription;
  private pointRemovingSubN: Subscription;
  constructor(
    @Inject('DomMapInstance') public globalMap: any,
    private _gs: GpxServiceService
    ) { }

  ngOnInit() {
    this.mapOptions = {
      layers: [
        new L.TileLayer("https://api.mapbox.com/styles/v1/mapbox/streets-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWluZGNvbm5lY3RhYiIsImEiOiJjajhjb3RxdjUwOHh6MndxbDFxb2YzOXY3In0.hdWlX6pFnQANfpHo6tVn-A", 
        {maxZoom: 18}),
        ],
      zoom: 12,
      center: L.latLng(59.32618430580267, 18.06289672851563),
      zoomControl: false,
      editable: true
    };
    this.formDataSubN = this._gs.getFormData().subscribe(data => this.formData = data);
    this.actionSubN = this._gs.getAction().subscribe(this.handleActions.bind(this));
    this.pointRemovingSubN = this._gs.getAddressPointToRemove().subscribe(this.handlePointRemoving.bind(this));
  }
  ngOnDestroy() {
    this.formDataSubN.unsubscribe();
    this.actionSubN.unsubscribe();
    this.pointRemovingSubN.unsubscribe();
  }
  onMapReady(map) {
    this.mapInstance = map;
    this.mapInstance['attributionControl'].setPrefix(
      '<a href="http://leafletjs.com/" target="_blank">Leaflet</a>' +
      ' | ' +
      '<a href="http://www.mindconnect.se/" target="_blank">Mindconnect</a>' +
      ' | ' +
      '<a href="https://www.openstreetmap.org/" target="_blank">OpenStreetMap</a>'
    );
    this.mapInstance['attributionControl'].setPosition('bottomleft');
    this.fileLoader = this.globalMap.Control['fileLayerLoad']({
      layer: this.globalMap.geoJson,
      fitBounds: true,
      layerOptions: {
        style: {
          color: '#88bc4f',
          weight: '5'
        }
      },
      addToMap: true,
      fileSizeLimit: 1024,
      formats: [
        '.geojson',
        '.kml'
      ]
    }).addTo(this.mapInstance);
    this.distanceMarker = L.marker([0, 0], {opacity: 0});
    this.fileLoader.loader.on('data:loaded', this.handleFileLoad.bind(this));
    this.mapInstance.fitWorld();
  }
  handleRouteMoving(e) {
    if (this.kmMarkersState) {
      for (let i = 0; i < this.mainRoute.kmMarkers.length; this.mapInstance.removeLayer(this.mainRoute.kmMarkers[i]), i += 1) { }
    }
    this.mainRoute.kmMarkers = this.createKmMarkers(this.mainRoute.getLatLngs());
    if (this.kmMarkersState) {
      for (let i = 0; i < this.mainRoute.kmMarkers.length; this.mapInstance.addLayer(this.mainRoute.kmMarkers[i]), i += 1) { }
    }
    this.pureRoute = this.createPurePolyline(this.mainRoute.getLatLngs());
    this._gs.setRecalculationState(false);
  }
  handleFileLoad(e) {
    this.mainRoute = e.layer.getLayers()['0'];
    this.mainRoute.kmMarkers = this.createKmMarkers(this.mainRoute.getLatLngs());

    this.pureRoute = this.createPurePolyline(this.mainRoute.getLatLngs());

    this.mainRoute.on('editable:vertex:dragend', this.handleRouteMoving.bind(this));
    this.mainRoute.on('mouseover', this.showDistance.bind(this));
    this.mainRoute.on('mouseout', this.hideDistance.bind(this));

    this.nearestModeState = true;
    this.kmMarkersState = true;
    this.polyMarkersState = true;
    this._gs.setAction(['nearest', 'kmmarkers', 'polymarkers']);
    this.handleNearestMode();
    this.handleKmMarkers();
    this.handlePolymarkers();
  }
  handleMapClick(e) {
    if (!this.mainRoute || !this.formData['valid']) {
      return;
    }
    this.handleClickMarkerAdding(e.latlng);
    const closestPoint = this.globalMap.GeometryUtil.closest(this.mapInstance, this.pureRoute, e.latlng);
    this._gs
      .getDataFromApi(`accept-language=en&format=json&addressdetails=1&zoom=18&lat=${e.latlng.lat}&lon=${e.latlng.lng}`)
      .subscribe(this.handleApiCall.bind(this, closestPoint));
  }
  handleClickMarkerAdding(latlng) {
    if (this.clickMarker) {
      this.clickMarker.removeFrom(this.mapInstance);
    }
    this.clickMarker = new L.Marker(latlng, {
      icon: new L.Icon({
        iconAnchor: [10, 30],
        iconSize: [20, 30],
        iconUrl: `assets/default_place.svg`,
        className: 'icon-holder'
      })})
      .addTo(this.mapInstance);
  }
  handleApiCall(latlng, response) {
    if (response.address && !response.address.road) {
      alert('Can\'t find street! Try another location');
      return;
    }

    const time = this.formData['time'].split(':');
    const hours = time['0'];
    const minutes = time['1'];
    const maxSpeed = this.formData['maxSpeed'];
    const minSpeed = this.formData['minSpeed'];

    const distanceFromStart = this.getDistance(this.pureRoute.distancePoints, latlng);
    this.markersCounter += 1;

    const markerData = {
      road: response.address.road,
      town: response.address.town || '',
      state: response.address.state || '',
      country: response.address.country || '',
      postcode: response.address.postcode || '',
      distance: distanceFromStart.toFixed(0),
      startTimeMax: this.getTime(new Date().setHours(hours, minutes, 0), minSpeed * 1000, distanceFromStart),
      startTimeMin: this.getTime(new Date().setHours(hours, minutes, 0), maxSpeed * 1000, distanceFromStart),
      raceTimeMin: this.getTime(new Date().setHours(0, 0, 0), maxSpeed * 1000, distanceFromStart),
      raceTimeMax: this.getTime(new Date().setHours(0, 0, 0), minSpeed * 1000, distanceFromStart),
      lat: latlng.lat.toFixed(5),
      lng: latlng.lng.toFixed(5),
      id: this.markersCounter
    };
    this.createClosestMarker(markerData);
    this.streetData.push(markerData);
    this._gs.setAddressPoint(markerData);
  }
  createPurePolyline(latlngs) {
    const pureRoute = L.polyline(latlngs.map(i => ({ lat: i.lat, lng: i.lng })));
    pureRoute['distancePoints'] = this.addDistanceForPoints(pureRoute.getLatLngs());
    return pureRoute;
  }
  createClosestMarker(markerData) {
    const divIconn = L.divIcon({
      html: `<img src="assets/imarker.svg"/><span class="index-marker-number">${markerData.id}</span>`,
      iconAnchor: [20, 40],
      iconSize: [41, 41],
      className: 'km-marker-common'
    });

    const closestMarker: any = L.marker(
      L.latLng(markerData.lat, markerData.lng),
      {
        icon: divIconn,
        draggable: false,
        opacity: 1,
      }
    );
    let htmlData = '';
    htmlData += `Index: #${markerData.id}<br />`;
    htmlData += `Road: ${markerData.road}<br />`;
    htmlData += `Distance (m): ${markerData.distance}<br />`;
    htmlData += `Time from start of race: ${markerData.startTimeMin} - ${markerData.startTimeMax}<br />`;
    htmlData += `Time from starting time: ${markerData.raceTimeMin} - ${markerData.raceTimeMax}<br />`;
    htmlData += `Lat: ${markerData.lat}<br />`;
    htmlData += `Lng: ${markerData.lng}`;
    closestMarker['dataId'] = markerData.id;
    closestMarker.bindTooltip(htmlData);
    if (this.polyMarkersState) {
      closestMarker.addTo(this.mapInstance);
    }
    this.closestMarkers.push(closestMarker);
  }
  getDistance(points, latlng) {
    let startDistance = 0;
    for (let i = 0; i < points.length; i += 1) {
      if (!points[i + 1]) {
        break;
      }
      const ifPointBelongTo = this.globalMap.GeometryUtil.belongsSegment(latlng, points[i].latLng, points[i + 1].latLng);
      if (!ifPointBelongTo) {
        continue;
      }
      const distanceToClosest = L.latLng(latlng).distanceTo(points[i].latLng);
      startDistance = points[i].distance + distanceToClosest;
      break;
    }
    return startDistance;
  }
  getTime(initialTime, speed, distance) {
    const timeInSeconds = (distance / (speed / 3600)).toFixed(0);
    const timestampDate = new Date(initialTime).setSeconds(parseInt(timeInSeconds, 10));
    const realDate = new Date(timestampDate);
    const hours = realDate.getHours();
    const minutes = realDate.getMinutes();
    return `${('0' + hours).slice(-2)}:${('0' + minutes).slice(-2)}`;
  }
  showDistance(e) {
    const points = this.pureRoute.distancePoints;
    const distanceFromStart = this.getDistance(points, e.latlng);

    const time = this.formData['time'].split(':');
    const hours = time['0'];
    const minutes = time['1'];
    const maxSpeed = this.formData['maxSpeed'];
    const minSpeed = this.formData['minSpeed'];

    const maxTime = this.getTime(new Date().setHours(hours, minutes, 0), minSpeed * 1000, distanceFromStart.toFixed(0));
    const minTime = this.getTime(new Date().setHours(hours, minutes, 0), maxSpeed * 1000, distanceFromStart.toFixed(0));
    const startMaxTime = this.getTime(new Date().setHours(0, 0, 0), minSpeed * 1000, distanceFromStart.toFixed(0));
    const startMinTime = this.getTime(new Date().setHours(0, 0, 0), maxSpeed * 1000, distanceFromStart.toFixed(0));

    let html = '';
    html += `Distance (m): ${distanceFromStart.toFixed(0)}<br />`;
    html += `Time from start of race: ${startMinTime} - ${startMaxTime}<br />`;
    html += `Time from starting time: ${minTime} - ${maxTime}<br />`;
    html += `Lat: ${e.latlng.lat.toFixed(5)}<br />`;
    html += `Lng: ${e.latlng.lng.toFixed(5)}`;
    this.distanceMarker.setLatLng(e.latlng).addTo(this.mapInstance).bindTooltip(html).openTooltip();
  }
  hideDistance() {
    this.distanceMarker.removeFrom(this.mapInstance);
  }
  createKmMarkers(points) {
    const markers = [];
    let kmNumber = 1;
    const formattedPoints = this.addDistanceForPoints(points);
    formattedPoints.reduce((segmentLength, currentPoint, arrIndex) => {
      if (!points[arrIndex + 1]) {
        return;
      }
      const distance = segmentLength + (formattedPoints[arrIndex + 1].distance - currentPoint.distance);
      if (distance > 1000) {
        const latUp = formattedPoints[arrIndex].latLng.lat > formattedPoints[arrIndex + 1].latLng.lat;
        const lngUp = formattedPoints[arrIndex].latLng.lng > formattedPoints[arrIndex + 1].latLng.lng;

        const differenceLat = latUp ?
          formattedPoints[arrIndex].latLng.lat - formattedPoints[arrIndex + 1].latLng.lat :
          formattedPoints[arrIndex + 1].latLng.lat - formattedPoints[arrIndex].latLng.lat;

        const differenceLng = lngUp ?
          formattedPoints[arrIndex].latLng.lng - formattedPoints[arrIndex + 1].latLng.lng :
          formattedPoints[arrIndex + 1].latLng.lng - formattedPoints[arrIndex].latLng.lng;

        const latMeterStep = (differenceLat / (formattedPoints[arrIndex + 1].distance - formattedPoints[arrIndex].distance));
        const lngMeterStep = (differenceLng / (formattedPoints[arrIndex + 1].distance - formattedPoints[arrIndex].distance));
        const segmentReminder = distance % 1000;

        const markersCounter = parseInt((distance / 1000).toString(), 10);

        let latStart = formattedPoints[arrIndex].latLng.lat;
        let lngStart = formattedPoints[arrIndex].latLng.lng;
        let markerLat, markerLng;
        let markerDivider = 1000 - segmentLength;

        for (let y = markersCounter; y >= 0;) {
          if (latUp) {
            markerLat = latStart - latMeterStep * markerDivider;
          } else {
            markerLat = latStart + latMeterStep * markerDivider;
          }

          if (lngUp) {
            markerLng = lngStart - lngMeterStep * markerDivider;
          } else {
            markerLng = lngStart + lngMeterStep * markerDivider;
          }

          const divIconn = L.divIcon({
            html: `<img src="assets/pcirlce.svg"/><span class="km-marker-number">${kmNumber}</span>`,
            iconAnchor: [17, 17],
            iconSize: [35, 35],
            className: 'km-marker-common'
          });

          const markerr = L.marker(
            [markerLat, markerLng],
            {
              icon: divIconn,
              draggable: false,
              opacity: 1,
            }
          );

          markers.push(markerr);
          kmNumber += 1;
          y -= 1;
          if (y === 0) {
            break;
          } else {
            latStart = markerLat;
            lngStart = markerLng;
            markerDivider = 1000;
          }
        }
        return segmentReminder;
      } else {
        return distance;
      }
    }, 0);
    return markers;
  }
  addDistanceForPoints(points) {
    const fPoints = [];
    fPoints.push({ latLng: points[0], distance: 0 });

    points.reduce((pointDistance, currentPoint, arrIndex) => {
      if (!arrIndex) { return 0; }
      pointDistance += L.latLng(points[arrIndex - 1]).distanceTo(points[arrIndex]);
      fPoints.push({ latLng: currentPoint, distance: pointDistance });
      return pointDistance;
    }, 0);
    return fPoints;
  }
  handleActions(action) {
    switch (action) {
      case 'zoomin':
        this.mapInstance.zoomIn();
        break;
      case 'zoomout':
        this.mapInstance.zoomOut();
        break;
      case 'nearest':
        this.nearestModeState = !this.nearestModeState;
        this.handleNearestMode();
        break;
      case 'kmmarkers':
        this.kmMarkersState = !this.kmMarkersState;
        this.handleKmMarkers();
        break;
      case 'polymarkers':
        this.polyMarkersState = !this.polyMarkersState;
        this.handlePolymarkers();
        break;
      case 'editpoly':
        this.editPolylineState = !this.editPolylineState;
        this.handlePolylineEdit();
        break;
      case 'csvexport':
        this.handleCsvExport();
        break;
      case 'gpxexport':
        this.handleGpxExport();
        break;
      case 'recalc':
      this.handleDataRecalculation();
      break;
    }
  }
  handleNearestMode() {
    if (!this.mainRoute) {
      return;
    }
    if (this.nearestModeState) {
      this.mapInstance.on('click', this.handleMapClick.bind(this));
    } else {
      if (this.clickMarker) {
        this.clickMarker.removeFrom(this.mapInstance);
      }
      this.mapInstance.off('click');
    }
  }
  handleKmMarkers() {
    if (!this.mainRoute) {
      return;
    }
    if (this.kmMarkersState) {
      for (let i = 0; i < this.mainRoute.kmMarkers.length; this.mapInstance.addLayer(this.mainRoute.kmMarkers[i]), i += 1) { }
    } else {
      for (let i = 0; i < this.mainRoute.kmMarkers.length; this.mapInstance.removeLayer(this.mainRoute.kmMarkers[i]), i += 1) { }
    }
  }
  handlePolymarkers() {
    if (!this.mainRoute) {
      return;
    }
    if (this.polyMarkersState) {
      for (let i = 0; i < this.closestMarkers.length; this.mapInstance.addLayer(this.closestMarkers[i]), i += 1) { }
    } else {
      for (let i = 0; i < this.closestMarkers.length; this.mapInstance.removeLayer(this.closestMarkers[i]), i += 1) { }
    }
  }
  handlePolylineEdit() {
    if (!this.mainRoute) {
      return;
    }
    this.editPolylineState ? this.mainRoute.enableEdit() : this.mainRoute.disableEdit();
  }
  handleCsvExport() {
    if (!this.streetData) {
      return;
    }
    const titleRows = [
      'road',
      'town',
      'state',
      'country',
      'postcode',
      'distance',
      'latest time form start of race',
      'earliest time form start of race',
      'latest time from starting time',
      'earliest time from starting time',
      'lat',
      'lng',
      'index'
    ];
    let csvContent = '';
    csvContent += `${titleRows.join(',')}\r\n`;

    this.streetData.forEach(function (item) {
      let valStr = '';
      for (const title in item) {
        if (item.hasOwnProperty(title)) {
          valStr += `${item[title]},`;
        }
      }
      valStr += '\r\n';
      csvContent += valStr;
    });

    const filename = 'Streets.csv'
    const link = document.createElement('a');
    link.download = 'Streets.csv';
    link.href = URL.createObjectURL(new File([csvContent], filename, { type: 'text/csv' }));
    link.click();
  }
  handleGpxExport() {
    if (!this.mainRoute) {
      return;
    }
    const doc = document.implementation.createDocument('', '', null);

    const gpxElem = doc.createElement('gpx');

    const metaElem = doc.createElement('metadata');
    const metaNameElem = doc.createElement('name');
    metaNameElem.innerHTML = 'name';
    metaElem.appendChild(metaNameElem);
    gpxElem.appendChild(metaElem);

    const trkElem = doc.createElement('trk');
    const trkNameElem = doc.createElement('name');
    trkNameElem.innerHTML = 'name';
    trkElem.appendChild(trkNameElem);

    const trksegElem = doc.createElement('trkseg');

    const latlngs = this.mainRoute.getLatLngs();

    for (let i = 0; i < latlngs.length; i += 1) {
      const trkptElem = doc.createElement('trkpt');
      trkptElem.setAttribute('lon', latlngs[i].lng);
      trkptElem.setAttribute('lat', latlngs[i].lat);

      const tName = doc.createElement('name');
      tName.innerHTML = 'Route point #' + i;
      trkptElem.appendChild(tName);

      trksegElem.appendChild(trkptElem);
    }

    trkElem.appendChild(trksegElem);
    gpxElem.appendChild(trkElem);
    doc.appendChild(gpxElem);

    const xmlSerializer = new XMLSerializer();
    let data = xmlSerializer.serializeToString(doc);
    data = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'${data}`;
    const filename = 'Layers.gpx';
    const link = document.createElement('a');
    link.download = filename;
    link.href = URL.createObjectURL(new File([data], filename, { type: 'text/xml' }));
    link.click();
  }
  handlePointRemoving(point) {
    this.streetData.splice(this.streetData.indexOf(point), 1);
    for (let i = 0; i < this.closestMarkers.length; i += 1) {
      if (this.closestMarkers[i]['dataId'] === point.id) {
        this.closestMarkers[i].removeFrom(this.mapInstance);
        this.closestMarkers.splice(i, 1);
        break;
      } else {
        continue;
      }
    }
  }
  handleDataRecalculation() {
    if (!this.streetData) {
      return;
    }
    const newData = this.streetData.map(i => i);
    this.streetData = [];
    newData.forEach(item => {
      const time = this.formData['time'].split(':');
      const hours = time['0'];
      const minutes = time['1'];
      const maxSpeed = this.formData['maxSpeed'];
      const minSpeed = this.formData['minSpeed'];

      const points = this.pureRoute['distancePoints'];
      const distanceFromStart = this.getDistance(points, L.latLng(item.lat, item.lng));

      const pointData = {
        road: item.road,
        town: item.town,
        state: item.state,
        country: item.country,
        postcode: item.postcode,
        distance: distanceFromStart.toFixed(0),
        startTimeMax: this.getTime(new Date().setHours(hours, minutes, 0), minSpeed * 1000, distanceFromStart),
        startTimeMin: this.getTime(new Date().setHours(hours, minutes, 0), maxSpeed * 1000, distanceFromStart),
        raceTimeMin: this.getTime(new Date().setHours(0, 0, 0), maxSpeed * 1000, distanceFromStart),
        raceTimeMax: this.getTime(new Date().setHours(0, 0, 0), minSpeed * 1000, distanceFromStart),
        lat: item.lat,
        lng: item.lng,
        id: item.id
      };
      this.streetData.push(pointData);
    });
    this.rebuildClosestMarkers();
    this._gs.setAddressRecalculationData(this.streetData);
    this._gs.setRecalculationState(true);
  }
  rebuildClosestMarkers() {
    if (!this.streetData.length)  {
      return;
    }
    for (let i = 0; i < this.closestMarkers.length; this.closestMarkers[i].removeFrom(this.mapInstance), i += 1) {}
    for (let i = 0; i < this.streetData.length; this.createClosestMarker(this.streetData[i]), i += 1) {}
  }
}
