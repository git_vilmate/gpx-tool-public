import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GpxServiceService {
  private formData = new BehaviorSubject<any>({});
  private addressPointSubject = new Subject<any>();
  private addressRecalculationSubject = new Subject<any>();
  private actionSubject = new Subject<any>();
  private recalculationState = new Subject<any>();
  private addressRemovingSubject = new Subject<any>();
  constructor(private _http: HttpClient) { }

  getFormData() {
    return this.formData.asObservable();
  }

  setFormData(data) {
    this.formData.next(data);
  }

  setAddressPoint(data) {
    this.addressPointSubject.next(data);
  }

  getAddressPoint() {
    return this.addressPointSubject.asObservable();
  }

  setAction(action) {
    this.actionSubject.next(action);
  }

  getAction() {
    return this.actionSubject.asObservable();
  }

  setRecalculationState(state) {
    this.recalculationState.next(state);
  }

  getrecalculationState() {
    return this.recalculationState.asObservable();
  }

  setAddressRecalculationData(data) {
    this.addressRecalculationSubject.next(data);
  }

  getAddressRecalculationData() {
    return this.addressRecalculationSubject.asObservable();
  }

  setAddressPointToRemove(point) {
    this.addressRemovingSubject.next(point);
  }
  getAddressPointToRemove() {
    return this.addressRemovingSubject.asObservable();
  }
  getDataFromApi(query) {
    return this._http.get(`https://nominatim.openstreetmap.org/reverse?${query}`);
  }
}
